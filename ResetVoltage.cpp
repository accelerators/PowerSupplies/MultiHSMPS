#include "ResetVoltage.hpp"
#include "PowerSupplyInfo.hpp"

#include <limits>
#include <regex>
#include <utility>

using ps::comeca::MagnetType;
using ps::comeca::PowerSupplyInfo;
using ps::comeca::PowerSupplyType;

namespace rstvolt {

parsing_result_t parseResetVoltage(const std::string &single_line) {
  constexpr const char *reset_voltage_regex_begin =
      "^(main|spare)\\s+(.*\\s+)?=\\s+";

  constexpr const char *decimal_number_regex =
      "[+-]?(?:[0-9]+\\.?[0-9]*|\\.[0-9]+)";

  static std::regex reset_voltage_main_regex(
      std::string(reset_voltage_regex_begin) + std::string("(") +
      std::string(decimal_number_regex) + std::string(")$"));

  PowerSupplyType out_type = PowerSupplyType::Unknown;
  ParsedRule out_rule;

  std::smatch match_result;
  if (std::regex_match(single_line, match_result, reset_voltage_main_regex)) {
    // The regex ensure a decimal point format -> no need to try/catch
    double set_point_parsed = std::stod(match_result[3].str());

    if (*match_result[1].first == 'm') {
      out_type = PowerSupplyType::Main;
      out_rule.main_setpoint = set_point_parsed;
    } else {
      out_type = PowerSupplyType::Spare;
      out_rule.spare_rule = Rule{.set_point = set_point_parsed,
                                 .magnet_type = MagnetType::Unknown,
                                 .spare_number = 0};

      // I separated the parsing in 2 steps (mandatory/optional) in order to
      // simplify the regex

      // The following part try to parse the 'optional' part of the ResetVoltage
      // string containing, for spares only, the Magnet Type and optionally the
      // spare number
      const auto optional_infos = match_result[2].str();

      if (not optional_infos.empty()) {

        constexpr const char *reset_voltage_optional_mag =
            "^(q|dq|so)\\s+(?:([0-9]+)\\s+)?$";

        static std::regex reset_voltage_opt_regex(reset_voltage_optional_mag);

        if (std::regex_match(optional_infos, match_result,
                             reset_voltage_opt_regex)) {
          // Keep in mind that currently this part of parsing only apply to
          // spares !
          switch (*match_result[1].first) {
          case 'q':
            out_rule.spare_rule.magnet_type = MagnetType::Q;
            break;
          case 'd':
            out_rule.spare_rule.magnet_type = MagnetType::DQ;
            break;
          case 's':
            out_rule.spare_rule.magnet_type = MagnetType::SO;
            break;
          default:
            out_rule.spare_rule.magnet_type = MagnetType::Unknown;
          }

          if (match_result[2].length() > 0) {
            // Regex ensure Integer format string -> no try/catch
            out_rule.spare_rule.spare_number =
                std::stoul(match_result[2].str());
          } else {
            out_rule.spare_rule.spare_number = 0;
          }
        } // regex_match()

      } // not optional_infos.empty()
    }
  }

  return std::make_pair(out_type, out_rule);
}

std::string format(const Rule &rule) {
  std::string out = ps::comeca::format(rule.magnet_type);
  if (rule.spare_number != 0) {
    out += " | Spare #" + std::to_string(rule.spare_number);
  }

  out += " = " + std::to_string(rule.set_point);
  return out;
}

/**
 *  \brief Stringify a Rulebook (for pretty prints)
 *
 *  \param[in] rule The Rulebook to print
 *  \param[in] rule_separator The use to separate the rule listing
 *  \return std::string A string version of the Rulebook
 */
std::string format(const RuleBook &rulebook) {
  std::string out = "Rulebook - Default: ";
  out += std::to_string(rulebook.default_setpoint);
  out += "\nRules:\n";
  for (std::size_t i = 0; i < rulebook.rules.size(); i++) {
    out += '[' + std::to_string(i) + "]: " + format(rulebook.rules[i]);
    out += '\n';
  }
  out.pop_back();
  return out;
}

bool match(const Rule &rule, const PowerSupplyInfo &ps_info) {
  if (rule.magnet_type != ps_info.magnet_type) {
    return false;
  }

  if (rule.spare_number == 0) {
    return true;
  } else {
    return rule.spare_number == ps_info.info.spare_number;
  }
}

} // namespace reset_voltage
