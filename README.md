# MultiHSMPS

Tango class to:
* summarize state/status of one cell main PS
* provide global On and Off commands

## Cloning

to clone this project, simply type:

```
git clone git@gitlab.esrf.fr:accelerators/PowerSupplies/MultiHSMPS.git
```

## Documentation 

Pogo generated documentation in **doc_html** folder

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++14 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

### Build

Instructions on building the project.

CMake example:

```bash
cd MultiHSMPS
mkdir -p build/debian9
cd build/debian9
cmake ../..
make
```
