# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [unreleased]

## [2.1.3] - 10-10-2023

### Fixed
 * fix reading attributes current, voltage when all device is enable.


## [2.1.2] - 26-06-2023

### Updated
 * add sleep time in off sequence just between off command to sub devices and resetting the current set point


## [2.1.1] - 01-06-2023

### Updated
 * fix reading attribute State, Connected, HSMInhibited enable all device in the group for that


## [2.1.0] - 31-05-2023

### added
 * add disable powersupply if HSMInhibited is true

### Updated
 * fix HSMinhibited return exception because Invalide value
    now return true if and only if HSMInhibited is true otherwise false

## [2.0.0] - 03-06-2022

### Removed
* onSequenceType and onSequenceSleeptTime attributes
  [ACU-513](https://jira.esrf.fr/browse/ACU-513)

### Added
* CmdOnSubGroupsSize and CmdOnDelayBetweenSubGroups properties
  [ACU-513](https://jira.esrf.fr/browse/ACU-513)
* New attribute 'Connected' (list all Connected for the underlying
  PSs)[ACU-513](https://jira.esrf.fr/browse/ACU-513)
* New attribute 'HSMInhibited' (list all Connected for the underlying
  PSs)[ACU-513](https://jira.esrf.fr/browse/ACU-513)
* TangoUtils.hpp + unit tests associated (utils function for Tango stuff)
  [ACU-513](https://jira.esrf.fr/browse/ACU-513)

### Updated
* Remove all POGO DEBUG logs that pollute the log viewer
  [ACU-513](https://jira.esrf.fr/browse/ACU-513)
* Remove set\_source(CACHE) stuff (now CACHE\_DEV all the time...)
  [ACU-513](https://jira.esrf.fr/browse/ACU-513)
* Refactor init by automatically cleaning up duplicate PSNames with only WARNING
  MSG [ACU-513](https://jira.esrf.fr/browse/ACU-513)
* On() command now use the PSNames list, sliced by onStride and delayed by
  onDelay [ACU-513](https://jira.esrf.fr/browse/ACU-513)
* Currents and Voltages attributes now output ALL PS (no more spare/connected
  filter) [ACU-513](https://jira.esrf.fr/browse/ACU-513)
* State/Status computation following
  [ACU-513](https://jira.esrf.fr/browse/ACU-513)

## [1.4.0] - 24-01-2022

### Added
* ResetVoltage Command & ResetVoltageSetpoint

## [1.2.5] - 21-10-2020

* Workaround a strange Tango behavior with group and command when
all device group memebr have a source set to CACHE

## [1.2.4] - 15-10-2020
* All device in group memeber with source set to CACHE in order to
not slow down the device when one PS is un-responsive

## [1.2.3] - 20-03-2020

### Fixed
* Bug in the attribuute PSStates: Wrong number of data returned

## [1.2.2] - 18-11-2019

### Changed
* In the global state, OFF has higher priority than ALARM

## [1.2.1] - 29-10-2019

### Changed
* State is now polled at 1 sec

## [1.2.0] - 21-10-2019

### Added
* Take into account that a channel may be in Tango DISABLE state
(meaning channel in DEBUG mode)

## [1.1.0] - 28-06-2019

### Added

* A thread is started in order to check the MacroServer door state in the
ON command. The rationale is to prevent timeout in case of long running
on sequence

## [1.0.1] - 27-06-2019

### Changed

* Change the min and max value for attribute OnSequenceSleepTime

## [1.0.0] - 03-06-2019

* First install in EBS control system

## [0.0.2] - 30-08-2018

### Added
* A new **CanBusPing** command

## [0.0.1] - 03-07-2018

Initial version
