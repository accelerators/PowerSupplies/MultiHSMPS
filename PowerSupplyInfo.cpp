#include "PowerSupplyInfo.hpp"

#include <algorithm>
#include <cassert>
#include <iterator>
#include <string>

namespace ps {
namespace comeca {
namespace helper {

/**
 *  \brief Small container used to store 2 iterators representing a range
 *  \tparam It The iterator type
 */
template <class It> struct Range {
  It begin;
  It end;
};

/**
 *  \brief Container used to store 3 ranges for the device name
 *  \tparam RangeIt The Range iterator type
 */
template <class RangeIt> struct DeviceNameRanges {
  Range<RangeIt> domain;
  Range<RangeIt> family;
  Range<RangeIt> member;
};

/**
 *  \brief Construct a DeviceName using a std::string
 *
 *  \param[in] device_name The device name to parse
 *  \return DeviceNameRanges<std::string::const_iterator>
 */
DeviceNameRanges<std::string::const_iterator>
getDeviceNameRanges(const std::string &device_name) noexcept {
  assert(std::count(device_name.cbegin(), device_name.cend(), '/') == 2 &&
         "Device name given is malformed, it should be something like "
         "\"domain/family/member\"");
  auto first_slash_index = device_name.find('/');
  auto second_slash_index = device_name.rfind('/');

  DeviceNameRanges<std::string::const_iterator> out;
  out.domain.begin = device_name.cbegin();
  out.domain.end = std::next(device_name.cbegin(), first_slash_index);

  out.family.begin = std::next(out.domain.end);
  out.family.end = std::next(device_name.cbegin(), second_slash_index);

  out.member.begin = std::next(out.family.end);
  out.member.end = device_name.cend();
  return out;
}

} // namespace helper

std::string format(const PowerSupplyType &ps_type) noexcept {
  switch (ps_type) {
  case PowerSupplyType::Main:
    return "MAIN";
  case PowerSupplyType::Spare:
    return "SPARE";
  default:
    return "UNKNOWN";
  }
}

std::string format(const MagnetType &magnet_type) noexcept {
  switch (magnet_type) {
  case MagnetType::DQ:
    return "Dipole-Quadripole";
  case MagnetType::Q:
    return "Quadripole";
  case MagnetType::S:
    return "Sextupole";
  case MagnetType::O:
    return "Octupole";
  case MagnetType::SO:
    return "Sextupole OR Octupole";
  default:
    return "UNKNOWN";
  }
}

std::string format(const MagnetInfo &magnet_info) noexcept {
  std::string out;
  out.reserve(5);

  out += std::to_string(magnet_info.number);
  out += "-";
  out += magnet_info.position;

  return out;
}

std::string format(const PowerSupplyInfo &ps_info) noexcept {
  std::string out = format(ps_info.type) + " PowerSupply";

  if (ps_info.type == PowerSupplyType::Main) {
    out += " - Magnet Type: " + format(ps_info.magnet_type);
    out += " (" + format(ps_info.info.magnet) + ")";
  } else if (ps_info.type == PowerSupplyType::Spare) {
    out += " (Spare number " + std::to_string(ps_info.info.spare_number) + ")";
    out += " - Magnet Type: " + format(ps_info.magnet_type);
  }

  return out;
}

PowerSupplyType getPSType(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept {
  // Currently the only way to know if a device is a spare or not is by checking
  // the name.
  // I hope it'll be changed in the near future
  return ps_proxy.dev_name().find("spare") != std::string::npos
             ? PowerSupplyType::Spare
             : PowerSupplyType::Main;
}

bool isHSMSparePS(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept {
  return getPSType(ps_proxy) == PowerSupplyType::Spare;
}

std::size_t getPSSpareNumber(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept {
  std::size_t spare_number;
  try {
    // Currently the spare number is at the end of the member field of the ps
    // device.

    // WARNING: strong assumption here: the spare number is ONLY 1 number
    spare_number = std::stoul(std::string(1, *ps_proxy.dev_name().rbegin()));
  } catch (const std::exception &err) {
    spare_number = 0;
  }
  return spare_number;
}

MagnetType getPSMagnetType(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept {
  // Currently the only way to know if a device is a suppose to be linked with a
  // Q, DQ, S or O magnet is by using the device name...
  const auto device_name = ps_proxy.dev_name();
  const auto device_name_ranges = helper::getDeviceNameRanges(device_name);
  const auto family_name = std::string(device_name_ranges.family.begin,
                                       device_name_ranges.family.end);

  // Following the current nomenclature, the family name of the PS is as follow:
  // -> start with 'ps-'
  // -> After, we have either qXN, sXN, oXN or dqN with X = f or d and N the
  // family number
  // -> in case of spare, name can be either ps-spare, ps-qspare or ps-dqspare.
  const auto magnet_type_index = family_name.find("ps-");

  // The family name is malformed
  if (magnet_type_index == std::string::npos)
    return MagnetType::Unknown;

  // The follwing method works only because the part after 'ps-' for spares
  // start with q, dq and s, which makes it generic, whether your are a spare or
  // not
  auto magnet_type_char_it =
      std::next(family_name.cbegin(), magnet_type_index + 3);

  switch (*magnet_type_char_it) {
  case 'd':
    return MagnetType::DQ;
  case 'q':
    return MagnetType::Q;
  case 's':
    // 's' may be because of sextupoles OR spare (ie: ps-spare or ps-sf1 or
    // ps-sd1...)
    // ps-spare is the spare for sextupoles/octupoles
    magnet_type_char_it++;
    if (*magnet_type_char_it == 'p') // This is the 'p' from 'spare'
      return MagnetType::SO;
    else
      return MagnetType::S;
  case 'o':
    return MagnetType::O;
  default:
    return MagnetType::Unknown;
  }
}

MagnetInfo getPSMagnetInfo(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept {
  // Currently the only way to know if the magnet family is by using the device
  // name...

  const auto device_name = ps_proxy.dev_name();
  const auto device_name_ranges = helper::getDeviceNameRanges(device_name);

  MagnetInfo out;
  try {
    // Currently the family number is located at the end of the family name of
    // the ps device.

    // WARNING: strong assumption here: the family number is ONLY 1 number
    auto magnet_number_string = std::string(device_name_ranges.family.end - 1,
                                            device_name_ranges.family.end);
    out.number = static_cast<unsigned char>(std::stoul(magnet_number_string));
  } catch (const std::exception &err) {
    out.number = 0;
  }
  out.position = device_name.back();

  return out;
}

PowerSupplyInfo getPSInfos(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept {
  PowerSupplyInfo out;
  out.type = getPSType(ps_proxy);
  out.magnet_type = getPSMagnetType(ps_proxy);

  switch (out.type) {
  case PowerSupplyType::Main:
    out.info.magnet = getPSMagnetInfo(ps_proxy);
    break;
  case PowerSupplyType::Spare:
    out.info.spare_number = getPSSpareNumber(ps_proxy);
    break;
  default:
    out.info.spare_number = 0;
  }
  return out;
}

} // namespace comeca
} // namespace ps
