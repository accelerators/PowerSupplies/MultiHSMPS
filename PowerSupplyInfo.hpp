/**
 *   \file PowerSupplyInfo.hpp
 *   \brief Contains functions use to retreive infos on COMECA PowerSupplies
 */
#pragma once

#include "tango.h" // Tango::DeviceProxy
#include <cstdlib> // std::size_t
#include <string>

namespace ps {
namespace comeca {

enum class PowerSupplyType { Main, Spare, Unknown };
enum class MagnetType { DQ, Q, S, O, SO, Unknown };

/**
 *  \brief Container storing the Magnet infos (number, position)
 */
struct MagnetInfo {
  unsigned char number;
  char position;
};

/**
 *  \brief Container storing all PS infos
 *
 *  PowerSupplyInfo store the informations about the power supply:
 *  - Its type (Main or Spare);
 *  - The magnet type its suppose to be connected to;
 *  - When it's a MAIN:
 *    - The magnet info (Number + Position a,b,c,d...)
 *  - When it's a SPARE:
 *    - The spare number
 */
struct PowerSupplyInfo {
  PowerSupplyType type;
  MagnetType magnet_type;

  union AdditionalInfo {
    std::size_t spare_number;
    MagnetInfo magnet;
  };
  AdditionalInfo info;
};

/**
 *  \brief Stringify a PowerSupplyType (for prints mainly)
 *
 *  \param[in] ps_type data to format
 *  \return std::string
 */
std::string format(const PowerSupplyType &ps_type) noexcept;

/**
 *  \brief Stringify a MagnetType (for prints mainly)
 *
 *  \param[in] magnet_type data to format
 *  \return std::string
 */
std::string format(const MagnetType &magnet_type) noexcept;

/**
 *  \brief Stringify a MagnetInfo (for prints mainly)
 *
 *  \param[in] magnet_family data to format
 *  \return std::string
 */
std::string format(const MagnetInfo &magnet_info) noexcept;

/**
 *  \brief Stringify a PowerSupplyInfo (for prints mainly)
 *
 *  \param[in] ps_info data to format
 *  \return std::string
 */
std::string format(const PowerSupplyInfo &ps_info) noexcept;

/**
 *  \brief Indicate if the given device is a Main or Spare powersupply
 *
 *  \param[in] ps_proxy the device proxy to look for
 *  \return PowerSupplyType Indicating if the PPS is a spare or not
 */
PowerSupplyType getPSType(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept;

/**
 *  \brief Indicate if the given device is a spare
 *
 *  \param[in] proxy The device proxy to look for
 *  \return bool true if the device is a spare power supply
 */
bool isHSMSparePS(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept;

/**
 *  \brief Retreive the spare number of a PS device
 *
 *  \param[in] ps_proxy The PS proxy to look for
 *  \return std::size_t the PS number (0 in case of parsing failure)
 */
std::size_t getPSSpareNumber(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept;

/**
 *  \brief Indicate if the given device is linked to a Q, DQ, S or O magnet
 *
 *  \param[in] ps_proxy The device proxy to look for
 *  \return ConnectedMagnetType Indicating the magnet Type retrieved
 */
MagnetType getPSMagnetType(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept;

/**
 *  \brief Retreive the magnet infos from the given ps proxy
 *
 *  \param[in] ps_proxy The ps proxy to look for
 *  \return MagnetInfo The magnet infos data struct retreived
 */
MagnetInfo getPSMagnetInfo(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept;

/**
 *  \brief Retreive the complete PS infos of a given PS device
 *
 *  TODO
 *
 *  \param[in] ps_proxy The PS device to look for
 *  \return PowerSupplyInfo The complete informations retreived from the proxy
 */
PowerSupplyInfo getPSInfos(/*const*/ Tango::DeviceProxy &ps_proxy) noexcept;

} // namespace comeca

} // namespace ps
