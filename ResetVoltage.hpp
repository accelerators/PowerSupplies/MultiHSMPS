/**
 *   \file ResetVoltage.hpp
 *   \brief Contains tools use to handle ResetVoltage parsing
 */
#pragma once

#include "PowerSupplyInfo.hpp"

#include <string>
#include <utility>
#include <vector>

namespace rstvolt {
/**
 *  \brief Contains data relative to a single rule
 */
struct Rule {
  double set_point;
  ps::comeca::MagnetType magnet_type;
  std::size_t spare_number;
};

/**
 *  \brief Union used by the parse function return type
 */
union ParsedRule {
  Rule spare_rule;
  double main_setpoint;
};

/// Result type of the parse function. Just an alias for user convenience.
using parsing_result_t = std::pair<ps::comeca::PowerSupplyType, ParsedRule>;

/**
 *  \brief Parse a ResetVoltageSetpoint line with a specific grammar
 *
 *  \param[in] single_line A string with a single line of ResetVoltageSetpoint
 *  \return A pair containing the PS type and an union based on the type
 */
parsing_result_t parseResetVoltage(const std::string &single_line);

/**
 *  \brief Contains all the rules parsed from the ResetVoltageSetpoint
 */
struct RuleBook {
  double default_setpoint;
  std::vector<Rule> rules;
};

/**
 *  \brief Create a complete Rulebook from multiple lines that will be parsed accordingly
 *
 *  \tparam StringInputIt Iterator type of std::string
 *  \param[in] first_line, last_line The first and last line to parse
 *  \param[in] default_setpoint The default setpoint assign to the rule book before parsing
 *  \return RuleBook The rule book created based on the parsing
 */
template <class StringInputIt>
RuleBook createRuleBookFrom(StringInputIt first_line, StringInputIt last_line,
                            double default_setpoint) {
  static_assert(
      std::is_same<typename std::iterator_traits<StringInputIt>::value_type,
                   std::string>::value,
      "createVoltageRuleBookFrom is only available for std::string iterators");

  RuleBook out_rulebook;
  out_rulebook.default_setpoint = default_setpoint;
  out_rulebook.rules.reserve(std::distance(first_line, last_line));

  for (; first_line != last_line; ++first_line) {
    ps::comeca::PowerSupplyType ps_type;
    ParsedRule parsed_rule;

    std::tie(ps_type, parsed_rule) = parseResetVoltage(*first_line);

    if (ps_type == ps::comeca::PowerSupplyType::Main)
      out_rulebook.default_setpoint = parsed_rule.main_setpoint;
    else if (ps_type == ps::comeca::PowerSupplyType::Spare)
      out_rulebook.rules.emplace_back(std::move(parsed_rule.spare_rule));
  }

  return out_rulebook;
}

/**
 *  \brief Stringify a Rule (for pretty prints)
 *
 *  \param[in] rule The Rule to print
 *  \return std::string A string version of the Rule
 */
std::string format(const Rule &rule);

/**
 *  \brief Stringify a Rulebook (for pretty prints)
 *
 *  \param[in] rule The Rulebook to print
 *  \return std::string A string version of the Rulebook
 */
std::string format(const RuleBook &rulebook);

/**
 *  \brief Indicates if the given ps_info matches with the given rule
 *
 *  \param[in] rule The rule to check for
 *  \param[in] ps_info The ps informations retreived
 *  \return bool True if the rule match with the ps given, false otherwise
 */
bool match(const Rule &rule, const ps::comeca::PowerSupplyInfo &ps_info);

} // namespace reset_voltage
