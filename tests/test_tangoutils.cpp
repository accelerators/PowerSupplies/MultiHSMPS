#include <algorithm>
#include <cstdint>
#include <iterator>
#include <limits>
#include <memory>
#include <ostream>
#include <random>
#include <string>
#include <type_traits>
#include <vector>

#include "TangoUtils.hpp"
#include "gtest/gtest.h"

namespace {

// Metaprogrammation nightmare, please close your eyes //////////////////////
// Sanity may decrease abnormally the longuer you try to understand/work on this
// s***. I don't even know why I keep doing this... I must be a masochist
namespace details {
template <class T,
          typename std::enable_if<std::is_arithmetic<T>::value, int>::type = 0>
struct get_uniform_distribution_of_impl {
  using type =
      typename std::conditional<std::is_floating_point<T>::value,
                                std::uniform_real_distribution<T>,
                                std::uniform_int_distribution<T>>::type;
};

// From a type T, extract U if T is define as T<U>, otherwise, return T
template <class T> struct get_inner_type_impl { using type = T; };

template <template <typename, typename...> class T, class U>
struct get_inner_type_impl<T<U>> {
  using type = U;
};

template <template <typename> class T, class U>
struct get_inner_type_impl<T<U>> {
  using type = U;
};

// This map a given type T<> with a list V (A, B, C, ...) contained into U
// From U<A, B, C> we get U<T<A>, T<B>, T<C>>
template <template <typename...> class T, class U> struct map_into_impl;

template <template <typename, typename...> class T,
          template <typename...> class U, class... V>
struct map_into_impl<T, U<V...>> {
  using type = U<T<V>...>;
};

// Concat 2 lists T<A, B, C> and T<D, E ,F> into T<A, B, C, D, E, F>
template <class T, class U> struct concat_impl;

template <template <typename...> class T, class... First, class... Second>
struct concat_impl<T<First...>, T<Second...>> {
  using type = T<First..., Second...>;
};

// This contains 'tests' for the metaprogrammation stuff...
namespace {
// Test get_uniform_distribution_of_impl
static_assert(std::is_same<typename get_uniform_distribution_of_impl<int>::type,
                           std::uniform_int_distribution<int>>::value,
              "Uniform distrib int failed for int");

static_assert(
    std::is_same<typename get_uniform_distribution_of_impl<long>::type,
                 std::uniform_int_distribution<long>>::value,
    "Uniform distrib int failed for long");

static_assert(
    std::is_same<typename get_uniform_distribution_of_impl<float>::type,
                 std::uniform_real_distribution<float>>::value,
    "Uniform distrib real failed for float");

static_assert(
    std::is_same<typename get_uniform_distribution_of_impl<double>::type,
                 std::uniform_real_distribution<double>>::value,
    "Uniform distrib real failed for double");

// Test get_inner_type_impl
using data_t = double;
using vector_t = std::vector<data_t>;

static_assert(
    std::is_same<typename get_inner_type_impl<data_t>::type, data_t>::value,
    "get_inner_type_impl failed to return T (identity function for non "
    "templated types)");

static_assert(not std::is_same<typename get_inner_type_impl<vector_t>::type,
                               vector_t>::value,
              "get_inner_type_impl returns std::vector<T> instead of T");

static_assert(
    std::is_same<typename get_inner_type_impl<vector_t>::type, data_t>::value,
    "get_inner_type_impl failed to retrieve the T from std::vector<T>");

template <class T> class Toto {};
using toto_t = Toto<data_t>;
static_assert(
    not std::is_same<typename get_inner_type_impl<toto_t>::type, toto_t>::value,
    "get_inner_type_impl returns Toto<T> instead of T");

static_assert(
    std::is_same<typename get_inner_type_impl<toto_t>::type, data_t>::value,
    "get_inner_type_impl failed to retrieve the T from Toto<T>");

// Test map_into_impl
using my_types = std::tuple<double, int>;
static_assert(
    std::is_same<typename map_into_impl<std::vector, my_types>::type,
                 std::tuple<std::vector<double>, std::vector<int>>>::value,
    "generate_type failed to append the correct types to std::vector");

// Test concat
static_assert(std::is_same<typename concat_impl<my_types, my_types>::type,
                           std::tuple<double, int, double, int>>::value,
              "concat failed to do stuffs...");

} // namespace

} // namespace details

/////////////////////////////////////////////////////////////////////////////
//                                  UTILS                                  //
/////////////////////////////////////////////////////////////////////////////
template <class T>
using uniform_distribution_t =
    typename details::get_uniform_distribution_of_impl<T>::type;

template <class T>
using value_type_of = typename details::get_inner_type_impl<T>::type;

template <template <typename...> class T, class U>
using map_into = typename details::map_into_impl<T, U>::type;

template <class T, class U>
using concat = typename details::concat_impl<T, U>::type;

// Types used for the TYPED_TEST  ///////////////////////////////////////////
using all_scalar_types =
    ::testing::Types<Tango::DevShort, Tango::DevLong, Tango::DevFloat,
                     Tango::DevDouble, Tango::DevUShort, Tango::DevULong,
                     Tango::DevState>;
using all_vector_types = map_into<std::vector, all_scalar_types>;
using all_types = concat<all_scalar_types, all_vector_types>;

// TODO: strings, State & co ? -> Must specialize generateRandomUniformSamples !

// Generator Functions /////////////////////////////////////////////////////////
// Following generate functions work for simple scalar type only
template <class T, class OutputIt, class Generator,
          typename std::enable_if<std::is_arithmetic<T>::value, int>::type = 0>
OutputIt
generateRandomUniformSamples(OutputIt d_first, std::size_t n,
                             Generator &rnd_generator,
                             const T &a = std::numeric_limits<T>::min(),
                             const T &b = std::numeric_limits<T>::max()) {
  uniform_distribution_t<T> distribution(a, b);
  return std::generate_n(d_first, n, [&rnd_generator, &distribution]() {
    return distribution(rnd_generator);
  });
};

template <class T, class OutputIt, class Generator,
          typename std::enable_if<std::is_same<T, Tango::DevState>::value,
                                  int>::type = 0>
OutputIt generateRandomUniformSamples(OutputIt d_first, std::size_t n,
                                      Generator &rnd_generator) {
  constexpr std::size_t number_of_states =
      std::extent<decltype(Tango::DevStateName)>::value;

  uniform_distribution_t<std::size_t> distribution(0, number_of_states - 1);
  return std::generate_n(d_first, n, [&rnd_generator, &distribution]() {
    return static_cast<Tango::DevState>(distribution(rnd_generator));
  });
};

template <class T, class OutputIt, class Generator,
          typename std::enable_if<not std::is_same<value_type_of<T>, T>::value,
                                  int>::type = 0>
OutputIt generateRandomUniformSamples(OutputIt d_first, std::size_t n,
                                      Generator &rnd_generator) {

  for (std::size_t i = 0; i != n; ++i) {
    T value_list;
    generateRandomUniformSamples<value_type_of<T>>(
        std::back_inserter(value_list), 10, rnd_generator);
    *d_first++ = std::move(value_list);
  }
  return d_first;
};

// Transfrom functions //////////////////////////////////////////////////////
template <class InputIt, class OutputIt>
void transformIntoValidGroupAttrReply(InputIt first, InputIt last,
                                      OutputIt d_first) {
  std::size_t i = 0;
  for (; first != last;) {
    std::string dev_name = std::to_string(i++);
    *d_first++ = Tango::GroupAttrReply(
        dev_name, "obj_name", Tango::DeviceAttribute("attr_name", *first++));
  }
}

template <class InputIt, class OutputIt>
void transformIntoValidGroupCmdReply(InputIt first, InputIt last,
                                     OutputIt d_first) {
  std::size_t i = 0;
  for (; first != last;) {
    std::string dev_name = std::to_string(i++);
    Tango::DeviceData tango_data;
    tango_data << *first++;
    *d_first++ = Tango::GroupCmdReply(dev_name, "obj_name", tango_data);
  }
}

template <class InputIt, class OutputIt>
void transformIntoDeviceAttribute(InputIt first, InputIt last,
                                  OutputIt d_first) {
  std::size_t i = 0;
  for (; first != last;) {
    std::string attr_name = std::to_string(i++);
    *d_first++ = Tango::DeviceAttribute(attr_name, *first++);
  }
}

template <class InputIt, class OutputIt>
void transformIntoDeviceData(InputIt first, InputIt last, OutputIt d_first) {
  for (; first != last;) {
    Tango::DeviceData tango_data;
    tango_data << *first++;
    *d_first++ = tango_data;
  }
}

// Print Functions //////////////////////////////////////////////////////////
std::ostream &operator<<(std::ostream &output, const Tango::DevState &state) {
  output << Tango::DevStateName[state];
  return output;
}

template <class InputIt,
          class ValueType = typename std::iterator_traits<InputIt>::value_type,
          typename std::enable_if<
              std::is_same<value_type_of<ValueType>, ValueType>::value,
              int>::type = 0>
std::string formatList(InputIt first, InputIt last) {
  std::stringstream out;
  out << "[";
  for (; first != last; ++first) {
    out << *first << ", ";
  }
  out << "]\n";
  return out.str();
};

template <class InputIt,
          class ValueType = typename std::iterator_traits<InputIt>::value_type,
          typename std::enable_if<
              not std::is_same<value_type_of<ValueType>, ValueType>::value,
              int>::type = 0>
std::string formatList(InputIt first, InputIt last) {
  std::string out = "{\n";
  for (; first != last; ++first) {
    out += formatList(std::begin(*first), std::end(*first));
  }
  out += "}\n";
  return out;
};

// Fixtures /////////////////////////////////////////////////////////////////
template <class T> class RndValueStoredTest : public testing::Test {
protected:
  std::vector<T> value_stored_;

  void SetUp() override {
    constexpr std::size_t number_of_samples = 10;

    // This seed is generated by GTest and can be set with
    // --gtest_random_seed=SEED command line OR with the GTEST_RANDOM_SEED env
    // variable
    auto seed = testing::UnitTest::GetInstance()->random_seed();
    // std::random_device rd_seed;
    // auto seed = rd_seed();

    std::cout << "Using seed: " << seed << std::endl;
    random_generator.seed(seed);

    // This templated function is necessary to differenciate between 'normal'
    // scalar types, where value_stored_ will only contains this random values,
    // and vector types, where values_stored_ will be fill with vectors of
    // random values
    value_stored_.reserve(number_of_samples);
    generateRandomUniformSamples<T>(std::back_inserter(value_stored_),
                                    number_of_samples, random_generator);
    std::cout << "Input value generated:\n"
              << formatList(std::begin(value_stored_), std::end(value_stored_));
  };

private:
  std::mt19937 random_generator;
};

template <class T> class RndDeviceAttribute : public RndValueStoredTest<T> {
protected:
  std::vector<Tango::DeviceAttribute> attribute_list_;
  void SetUp() override {
    RndValueStoredTest<T>::SetUp();
    attribute_list_.reserve(this->value_stored_.size());
    transformIntoDeviceAttribute(this->value_stored_.begin(),
                                 this->value_stored_.end(),
                                 std::back_inserter(attribute_list_));
  }
};

template <class T> class RndGroupAttrReply : public RndValueStoredTest<T> {
protected:
  Tango::GroupAttrReplyList reply_list_;
  void SetUp() override {
    RndValueStoredTest<T>::SetUp();
    reply_list_.reserve(this->value_stored_.size());
    transformIntoValidGroupAttrReply(this->value_stored_.begin(),
                                     this->value_stored_.end(),
                                     std::back_inserter(reply_list_));
  }
};

/////////////////////////////////////////////////////////////////////////////
//                                  TESTS                                  //
/////////////////////////////////////////////////////////////////////////////
TEST(TestTangoUtils, EmptyInputRange) {
  Tango::GroupAttrReplyList replies;
  std::vector<double> value_copied;

  std::vector<double> invalid_value_copied(
      replies.size(), std::numeric_limits<double>::infinity());

  std::vector<int> invalid_list;
  auto format = [](Tango::GroupAttrReply &invalid_reply) { return -1; };

  {
    auto out = utils::extractUntil(
        replies.begin(), replies.end(), value_copied.begin(),
        [](Tango::GroupAttrReply &reply) { return reply.has_failed(); });

    ASSERT_EQ(out.first, replies.end());
    ASSERT_EQ(out.first, replies.begin());
    ASSERT_EQ(out.second, value_copied.begin());

    auto out_2 = utils::extractUntil<double>(
        replies.begin(), replies.end(), std::back_inserter(value_copied),
        [](Tango::GroupAttrReply &reply) { return reply.has_failed(); });

    ASSERT_EQ(out_2.first, replies.end());
    ASSERT_EQ(out_2.first, replies.begin());
    ASSERT_TRUE(value_copied.empty());
  }

  {
    auto out = utils::extractUntilRepliesInvalid(replies.begin(), replies.end(),
                                                 value_copied.begin());

    ASSERT_EQ(out.first, replies.end());
    ASSERT_EQ(out.first, replies.begin());
    ASSERT_EQ(out.second, value_copied.begin());

    auto out_2 = utils::extractUntilRepliesInvalid<double>(
        replies.begin(), replies.end(), std::back_inserter(value_copied));

    ASSERT_EQ(out_2.first, replies.end());
    ASSERT_EQ(out_2.first, replies.begin());
    ASSERT_TRUE(value_copied.empty());
  }

  {
    auto out = utils::extractRepliesAndInvalid(
        replies.begin(), replies.end(), value_copied.begin(),
        std::back_inserter(invalid_list), format);

    ASSERT_EQ(out.first, value_copied.begin());
    ASSERT_TRUE(invalid_list.empty());

    utils::extractRepliesAndInvalid<double>(
        replies.begin(), replies.end(), std::back_inserter(value_copied),
        std::back_inserter(invalid_list), format);

    ASSERT_TRUE(invalid_list.empty());
    ASSERT_TRUE(value_copied.empty());
  }

  {
    auto out = utils::extractRepliesAndInvalid(
        replies.begin(), replies.end(), value_copied.begin(),
        std::back_inserter(invalid_list), format, -1.5);

    ASSERT_EQ(out.first, value_copied.begin());
    ASSERT_TRUE(invalid_list.empty());

    utils::extractRepliesAndInvalid(
        replies.begin(), replies.end(), std::back_inserter(value_copied),
        std::back_inserter(invalid_list), format, -1.5);

    ASSERT_TRUE(value_copied.empty());
    ASSERT_TRUE(invalid_list.empty());
  }

  {
    auto out = utils::partitionExtract(
        replies.begin(), replies.end(), value_copied.begin(),
        invalid_value_copied.begin(), [](Tango::GroupAttrReply &reply) {
          return reply.dev_name() == "CHOCOLATINE";
        });

    ASSERT_EQ(out.first, value_copied.begin());
    ASSERT_EQ(out.second, invalid_value_copied.begin());

    utils::partitionExtract<double, double>(
        replies.begin(), replies.end(), std::back_inserter(value_copied),
        std::back_inserter(invalid_value_copied),
        [](Tango::GroupAttrReply &reply) {
          return reply.dev_name() == "CHOCOLATINE";
        });

    ASSERT_TRUE(value_copied.empty());
    ASSERT_TRUE(invalid_value_copied.empty());
  }
}

/////////////////////////////////////////////////////////////////////////////
//                               ExtractUntil                              //
/////////////////////////////////////////////////////////////////////////////
template <class T> using ExtractUntilDeviceAttr = RndDeviceAttribute<T>;
TYPED_TEST_SUITE_P(ExtractUntilDeviceAttr);
TYPED_TEST_P(ExtractUntilDeviceAttr, UntilEnd) {

  decltype(this->value_stored_) value_copied;
  auto out = utils::extractUntil<TypeParam>(
      this->attribute_list_.begin(), this->attribute_list_.end(),
      std::back_inserter(value_copied),
      [](Tango::DeviceAttribute &attr) { return false; });

  ASSERT_EQ(out.first, this->attribute_list_.end());
  ASSERT_EQ(value_copied.size(), this->attribute_list_.size());
  ASSERT_EQ(value_copied.size(), this->value_stored_.size());
  ASSERT_EQ(value_copied, this->value_stored_);
}

TYPED_TEST_P(ExtractUntilDeviceAttr, UntilMiddle) {

  const std::size_t stop_index = this->attribute_list_.size() / 2;
  const auto &stop_attr = this->attribute_list_[stop_index];

  decltype(this->value_stored_) value_copied;
  auto out = utils::extractUntil<TypeParam>(
      this->attribute_list_.begin(), this->attribute_list_.end(),
      std::back_inserter(value_copied),
      [&stop_attr](Tango::DeviceAttribute &attr) {
        return attr.name == stop_attr.name;
      });

  ASSERT_NE(out.first, this->attribute_list_.end());

  ASSERT_EQ(out.first->name, stop_attr.name);

  ASSERT_NE(value_copied.size(), this->attribute_list_.size());
  ASSERT_NE(value_copied.size(), this->value_stored_.size());
  ASSERT_NE(value_copied.size(), stop_index + 1);

  std::size_t i = 0;
  for (; i != stop_index; ++i)
    ASSERT_EQ(value_copied[i], this->value_stored_[i]);
}
REGISTER_TYPED_TEST_SUITE_P(ExtractUntilDeviceAttr, UntilEnd, UntilMiddle);

INSTANTIATE_TYPED_TEST_SUITE_P(TestExtractUntil, ExtractUntilDeviceAttr,
                               all_types);

template <class T> using ExtractUntilGroupAttrRepl = RndGroupAttrReply<T>;
TYPED_TEST_SUITE_P(ExtractUntilGroupAttrRepl);
TYPED_TEST_P(ExtractUntilGroupAttrRepl, UntilEnd) {
  {
    decltype(this->value_stored_) value_copied(this->reply_list_.size());

    auto out =
        utils::extractUntil(this->reply_list_.begin(), this->reply_list_.end(),
                            value_copied.begin(),
                            [](Tango::GroupAttrReply &reply) { return false; });

    ASSERT_EQ(out.first, this->reply_list_.end());
    ASSERT_EQ(out.second, value_copied.end());
    ASSERT_EQ(value_copied, this->value_stored_);
  }

  {
    decltype(this->value_stored_) value_copied;
    auto out = utils::extractUntil<TypeParam>(
        this->reply_list_.begin(), this->reply_list_.end(),
        std::back_inserter(value_copied),
        [](Tango::GroupAttrReply &reply) { return false; });

    ASSERT_EQ(out.first, this->reply_list_.end());
    ASSERT_EQ(value_copied.size(), this->reply_list_.size());
    ASSERT_EQ(value_copied.size(), this->value_stored_.size());
    ASSERT_EQ(value_copied, this->value_stored_);
  }
}

TYPED_TEST_P(ExtractUntilGroupAttrRepl, UntilMiddle) {
  decltype(this->value_stored_) value_copied(this->reply_list_.size());

  constexpr std::size_t stop_copy_idx = 3;
  const auto stop_reply = this->reply_list_[stop_copy_idx];
  auto out = utils::extractUntil(
      this->reply_list_.begin(), this->reply_list_.end(), value_copied.begin(),
      [&stop_reply](Tango::GroupAttrReply &reply) {
        return reply.dev_name() == stop_reply.dev_name();
      });

  ASSERT_NE(value_copied, this->value_stored_);

  ASSERT_NE(out.first, this->reply_list_.end());
  ASSERT_EQ(out.first->dev_name(), stop_reply.dev_name());
  ASSERT_EQ(out.first->obj_name(), stop_reply.obj_name());

  ASSERT_NE(out.second, value_copied.end());

  ASSERT_EQ(std::distance(value_copied.begin(), out.second), stop_copy_idx);

  std::size_t i = 0;
  for (; i != stop_copy_idx; ++i)
    ASSERT_EQ(value_copied[i], this->value_stored_[i]);

  for (; i != this->value_stored_.size(); ++i)
    ASSERT_NE(value_copied[i], this->value_stored_[i]);
}

REGISTER_TYPED_TEST_SUITE_P(ExtractUntilGroupAttrRepl, UntilEnd, UntilMiddle);
INSTANTIATE_TYPED_TEST_SUITE_P(TestExtractUntil, ExtractUntilGroupAttrRepl,
                               all_types);

/////////////////////////////////////////////////////////////////////////////
//                             PartitionExtract                            //
/////////////////////////////////////////////////////////////////////////////
template <class T> using PartitionExtractDeviceAttr = RndDeviceAttribute<T>;
TYPED_TEST_SUITE_P(PartitionExtractDeviceAttr);
TYPED_TEST_P(PartitionExtractDeviceAttr, DeviceAttrName) {
  decltype(this->value_stored_) value_copied_odd, value_copied_even;

  const auto attr_name_is_even = [](Tango::DeviceAttribute &attr) {
    // Test the first char of the attr name
    return ((attr.name.front() % 2) == 0);
  };

  utils::partitionExtract<TypeParam, TypeParam>(
      this->attribute_list_.begin(), this->attribute_list_.end(),
      std::back_inserter(value_copied_even),
      std::back_inserter(value_copied_odd), attr_name_is_even);

  ASSERT_EQ(this->attribute_list_.size(),
            (value_copied_even.size() + value_copied_odd.size()));

  auto odd_it = value_copied_odd.cbegin();
  auto even_it = value_copied_even.cbegin();
  auto expected_value_it = this->value_stored_.cbegin();
  for (auto &attr : this->attribute_list_) {
    if (attr_name_is_even(attr)) {
      ASSERT_EQ(*even_it++, *expected_value_it++);
    } else {
      ASSERT_EQ(*odd_it++, *expected_value_it++);
    }
  }
}
REGISTER_TYPED_TEST_SUITE_P(PartitionExtractDeviceAttr, DeviceAttrName);
INSTANTIATE_TYPED_TEST_SUITE_P(TestPartitionExtract, PartitionExtractDeviceAttr,
                               all_types);

/////////////////////////////////////////////////////////////////////////////
//                        extractUntilRepliesInvalid                       //
/////////////////////////////////////////////////////////////////////////////
using TestExtractUntilRepliesInvalid = RndGroupAttrReply<int>;
TEST_F(TestExtractUntilRepliesInvalid, UntilMiddle) {
  // We insert an error in the middle
  const std::size_t error_index = reply_list_.size() / 2;
  reply_list_[error_index] =
      Tango::GroupAttrReply("Robert", "Yep", Tango::DevFailed());

  std::vector<int> value_copied(reply_list_.size());
  auto out = utils::extractUntilRepliesInvalid(
      reply_list_.begin(), reply_list_.end(), value_copied.begin());

  ASSERT_NE(value_copied, value_stored_);

  ASSERT_NE(out.first, reply_list_.end());
  ASSERT_EQ(out.first->dev_name(), "Robert");
  ASSERT_EQ(out.first->obj_name(), "Yep");

  ASSERT_NE(out.second, value_copied.end());

  ASSERT_EQ(std::distance(value_copied.begin(), out.second), error_index);

  std::size_t i = 0;
  for (; i != error_index; ++i)
    ASSERT_EQ(value_copied[i], value_stored_[i]);

  for (; i != value_stored_.size(); ++i)
    ASSERT_NE(value_copied[i], value_stored_[i]);
}

/////////////////////////////////////////////////////////////////////////////
//                         extractRepliesAndInvalid                        //
/////////////////////////////////////////////////////////////////////////////
using TestExtractRepliesAndInvalid = RndGroupAttrReply<int>;
TEST_F(TestExtractRepliesAndInvalid, ErrorAndQualityWithoutDefault) {
  using data_t = int;

  // We insert an error in the middle
  const std::size_t error_index = reply_list_.size() / 2;
  auto error_reply_it = std::next(reply_list_.begin(), error_index);
  Tango::DevErrorList error_list_created;
  error_list_created.length(1);
  error_list_created[0].severity = Tango::ERR;
  error_list_created[0].origin = Tango::string_dup("Mount Olympus");
  error_list_created[0].reason = Tango::string_dup("Zeus angry !");
  *error_reply_it = Tango::GroupAttrReply("Robert", "Yep",
                                          Tango::DevFailed(error_list_created));

  // Set the last reply as ATTR_INVALID
  auto invalid_reply_it = std::next(reply_list_.begin(), reply_list_.size());
  invalid_reply_it->get_data().quality = Tango::ATTR_INVALID;

  std::vector<data_t> valid_value_copied;
  valid_value_copied.reserve(reply_list_.size());

  using invalid_t = std::string;
  const auto create_invalid_value = [](Tango::GroupAttrReply &reply) {
    std::string out_msg = "Chocolatine " + reply.dev_name() + ": ";
    if (reply.has_failed()) {
      out_msg += reply.get_err_stack()[0].desc.in();
    } else {
      out_msg += "INVALID";
    }

    return out_msg;
  };

  std::vector<invalid_t> invalid_value_created;
  invalid_value_created.reserve(reply_list_.size());

  utils::extractRepliesAndInvalid<data_t>(
      reply_list_.begin(), reply_list_.end(),
      std::back_inserter(valid_value_copied),
      std::back_inserter(invalid_value_created), create_invalid_value);

  // Can't test out with back_inserter...

  ASSERT_EQ(reply_list_.size(),
            (valid_value_copied.size() + invalid_value_created.size()));

  auto value_stored_it = value_stored_.cbegin();
  auto valid_value_copied_it = valid_value_copied.cbegin();
  auto invalid_value_created_it = invalid_value_created.cbegin();
  for (auto reply_it = reply_list_.begin(); reply_it != reply_list_.end();
       ++reply_it, ++value_stored_it) {
    if (reply_it == error_reply_it or reply_it == invalid_reply_it) {
      ASSERT_EQ(*invalid_value_created_it++, create_invalid_value(*reply_it));
    } else {
      ASSERT_EQ(*valid_value_copied_it++, *value_stored_it);
    }
  }
}

TEST_F(TestExtractRepliesAndInvalid, IntErrorAndQualityWithDefault) {
  using data_t = int;

  // We insert an error in the middle
  const std::size_t error_index = reply_list_.size() / 2;
  auto error_reply_it = std::next(reply_list_.begin(), error_index);
  Tango::DevErrorList error_list_created;
  error_list_created.length(1);
  error_list_created[0].severity = Tango::ERR;
  error_list_created[0].origin = Tango::string_dup("Mount Olympus");
  error_list_created[0].reason = Tango::string_dup("Athena angry !");
  *error_reply_it = Tango::GroupAttrReply("Robert", "Yep",
                                          Tango::DevFailed(error_list_created));

  // Set the last reply as ATTR_INVALID
  auto invalid_reply_it = std::next(reply_list_.begin(), reply_list_.size());
  invalid_reply_it->get_data().quality = Tango::ATTR_INVALID;

  constexpr data_t default_value_when_invalid = -1;
  std::vector<data_t> valid_value_copied;
  valid_value_copied.reserve(reply_list_.size());

  using invalid_t = std::string;
  const auto create_invalid_value = [](Tango::GroupAttrReply &reply) {
    std::string out_msg = "Croissant " + reply.dev_name() + ": ";
    if (reply.has_failed()) {
      out_msg += reply.get_err_stack()[0].desc.in();
    } else {
      out_msg += "INVALID";
    }

    return out_msg;
  };

  std::vector<invalid_t> invalid_value_created;
  invalid_value_created.reserve(reply_list_.size());

  utils::extractRepliesAndInvalid<data_t>(
      reply_list_.begin(), reply_list_.end(),
      std::back_inserter(valid_value_copied),
      std::back_inserter(invalid_value_created), create_invalid_value,
      default_value_when_invalid);

  // Can't test out with back_inserter...
  ASSERT_EQ(reply_list_.size(), valid_value_copied.size());

  auto value_stored_it = value_stored_.cbegin();
  auto valid_value_copied_it = valid_value_copied.cbegin();
  auto invalid_value_created_it = invalid_value_created.cbegin();
  for (auto reply_it = reply_list_.begin(); reply_it != reply_list_.end();
       ++reply_it, ++value_stored_it, ++valid_value_copied_it) {
    if (reply_it == error_reply_it or reply_it == invalid_reply_it) {
      ASSERT_EQ(*invalid_value_created_it++, create_invalid_value(*reply_it));
      ASSERT_EQ(*valid_value_copied_it, default_value_when_invalid);
    } else {
      ASSERT_EQ(*valid_value_copied_it, *value_stored_it);
    }
  }
}

} // namespace
