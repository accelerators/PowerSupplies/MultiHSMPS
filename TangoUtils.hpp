/**
 *   \file TangoUtils.hpp
 *   \brief Contains utils functions acting on Tango stuff
 */
#pragma once

#include <iterator>    // iterator_traits<>
#include <tuple>       // std::tie
#include <type_traits> // mataprogr stuffs
#include <utility>     // pair<>

#include "tango.h" // Tango::DeviceProxy

namespace utils {
/**
 *  \brief Exctract Tango elements (i.e. declare the operator>>() for
 *         extraction) from [first, last) into d_first until pred returns true.
 *
 *  \note An issue create a compilation error when using
 *        std::back_insert_iterator (any kind of iterator adapter ?). This is
 *        because extracting data from Tango stuffs is done through the
 *        operator>>(), and not the classical assignement operator=(). Since
 *        OutputIterators are only defined when dereferencing and incrementing.
 *        Problem is, when dereferencing a back_insert_iterator, it returns a
 *        back_insert_iterator, and not the normal type, which is not overloaded
 *        in the operator>> of DeviceAttribute.
 *
 *  \tparam InputIt LegacyInputIterator (value_type must be define operator>>())
 *  \tparam OutputIt LegacyOutputIterator used to extract data into
 *  \tparam UnaryPredicate The function object of type bool = (*first)
 *
 *  \param[in] first, last The input iterator range of replies to copy
 *  \param[out] d_first The destination iterator range where to copy the data in
 *  \param[in] pred Unary predicate taking an element as argument and returning
                    true when we should stop copying.
 *  \return pair<InputIt, OutputIt> Containing the first and d_first iterator
 *                                  where it stopped
 */
template <class InputIt, class OutputIt, class UnaryPredicate>
std::pair<InputIt, OutputIt> extractUntil(InputIt first, InputIt last,
                                          OutputIt d_first,
                                          UnaryPredicate pred) {

  for (; first != last and not pred(*first);) {
    *first++ >> *d_first++;
  }
  return std::make_pair(first, d_first);
}

/**
 *  \brief Overload of extractUntil when specifying the extracted type
 *  (LegacyOutputIterator friendly)
 *
 *  \note This is the only way to use iterator adapters like
 *  std::back_inserter() in order to manually tell DeviceAttribute the output
 *  type since OutputIterators doesn't have one...
 *
 *  \tparam InputIt LegacyInputIterator (value_type must be define operator>>())
 *  \tparam OutputIt LegacyOutputIterator used to extract data into
 *  \tparam UnaryPredicate The function object of type bool = (*first)
 *  \tparam DataType The type used for extraction before d_first insertion
 *
 *  \param[in] first, last The input iterator range of replies to copy
 *  \param[out] d_first The destination iterator range where to copy the data in
 *  \param[in] pred Unary predicate taking an element as argument and returning
                    true when we should stop copying.
 *  \return pair<InputIt, OutputIt> Containing the first and d_first iterator
 *                                  where it stopped
 */
template <class DataType, class InputIt, class OutputIt, class UnaryPredicate>
std::pair<InputIt, OutputIt> extractUntil(InputIt first, InputIt last,
                                          OutputIt d_first,
                                          UnaryPredicate pred) {

  for (; first != last and not pred(*first);) {
    DataType extracted_data;
    *first++ >> extracted_data;
    *d_first++ = extracted_data;
  }
  return std::make_pair(first, d_first);
}

/**
 *  \brief Extract Tango elements (ie. operator>>() used for extraction) from
 *         [first, last) into two different ranges, based on the boolean
 *         returned by `pred`. Replies satisfying `pred` will be copied into
 *         `d_first_true`, the rest will be copied into `d_first_false`.
 *
 *  This mimics the std::partition_copy() function from the STL in order to
 *  partition tango stuffs that require operator>>() for extraction. It may be
 *  used to easily partition replies when reading mutliple attributes, for
 *  example.
 *
 *  \tparam InputIt LegacyInputIterator (value_type must be define operator>>())
 *  \tparam OutputIt1 LegacyOutputIterator used to extract data into when pred()
 *                    returns true
 *  \tparam OutputIt2 LegacyOutputIterator used to extract data into when pred()
 *                    returns false
 *  \tparam UnaryPredicate The function object of type
 *                         bool = (InputIt::value_type&)
 *
 *  \param[in] first, last The input iterator range of replies to copy
 *  \param[out] d_first_true The destination iterator range where to copy the
 *                           data in if pred is true.
 *  \param[out] d_first_false The destination iterator range where to copy the
 *                            data in if pred is false.
 *  \param[in] pred Unary predicate taking an element as argument and returning
 *                  true when we should copy data into d_first_true.
 *  \return pair<OutputIt1, OutputIt2> A pair constructed from the iterator to
 *                                     the end of d_first_true and d_first_false
 *                                     range.
 */
template <class InputIt, class OutputIt1, class OutputIt2, class UnaryPredicate>
std::pair<OutputIt1, OutputIt2>
partitionExtract(InputIt first, InputIt last, OutputIt1 d_first_true,
                 OutputIt2 d_first_false, UnaryPredicate pred) {

  using value_t = typename std::iterator_traits<InputIt>::value_type;
  const auto not_pred = [&pred](value_t &element) { return not pred(element); };

  while (first != last) {
    std::tie(first, d_first_true) =
        extractUntil(first, last, d_first_true, not_pred);

    std::tie(first, d_first_false) =
        extractUntil(first, last, d_first_false, pred);
  }

  return std::make_pair(d_first_true, d_first_false);
}

/**
 *  \brief partitionExtract overload specifying the extracted type
 *  (LegacyOutputIterator friendly) for both outputs.
 *
 *  \tparam InputIt LegacyInputIterator (value_type must be define operator>>())
 *  \tparam OutputIt1 LegacyOutputIterator used to extract data into when pred()
 *                    returns true
 *  \tparam OutputIt2 LegacyOutputIterator used to extract data into when pred()
 *                    returns false
 *  \tparam UnaryPredicate The function object of type
 *                         bool = (InputIt::value_type&)
 *  \tparam DataType1 The type used for extract before d_first_true insertion
 *  \tparam DataType2 The type used for extract before d_first_false insertion
 *
 *  \param[in] first, last The input iterator range of replies to copy
 *  \param[out] d_first_true The destination iterator range where to copy the
 *                           data in if pred is true.
 *  \param[out] d_first_false The destination iterator range where to copy the
 *                            data in if pred is false.
 *  \param[in] pred Unary predicate taking an element as argument and returning
 *                  true when we should copy data into d_first_true.
 *  \return pair<OutputIt1, OutputIt2> A pair constructed from the iterator to
 *                                     the end of d_first_true and d_first_false
 *                                     range.
 */
template <class DataType1, class DataType2, class InputIt, class OutputIt1,
          class OutputIt2, class UnaryPredicate>
std::pair<OutputIt1, OutputIt2>
partitionExtract(InputIt first, InputIt last, OutputIt1 d_first_true,
                 OutputIt2 d_first_false, UnaryPredicate pred) {

  using value_t = typename std::iterator_traits<InputIt>::value_type;
  const auto not_pred = [&pred](value_t &element) { return not pred(element); };

  while (first != last) {
    std::tie(first, d_first_true) =
        extractUntil<DataType1>(first, last, d_first_true, not_pred);

    std::tie(first, d_first_false) =
        extractUntil<DataType2>(first, last, d_first_false, pred);
  }

  return std::make_pair(d_first_true, d_first_false);
}

// Following algorithms works with Tango::GroupReply ONLY //////////////////////
/**
 *  \brief Extract Tango::GroupReply elements from [first, last) into d_first
 *         until a reply failed (has_failed() == true OR reply quality ==
 *         ATTR_INVALID).
 *
 *  \tparam InputIt LegacyInputIterator (::value_type must be a
 *                  Tango::GroupReply)
 *  \tparam OutputIt LegacyOutputIterator used to extract data into
 *
 *  \param[in] first, last The input iterator range of replies to copy
 *  \param[out] d_first The destination iterator range where to copy the data in
 *  \return pair<InputIt, OutputIt> Containing the first and d_first iterator
 *                                  where it stopped
 */
template <class InputIt, class OutputIt>
std::pair<InputIt, OutputIt>
extractUntilRepliesInvalid(InputIt first, InputIt last, OutputIt d_first) {
  using reply_t = typename std::iterator_traits<InputIt>::value_type;

  static_assert(std::is_base_of<Tango::GroupReply, reply_t>::value,
                "extractRepliesUntilInvalid is only availaible for "
                "Tango::GroupReply inputs");

  return extractUntil(first, last, d_first, [](reply_t &reply) {
    return (reply.has_failed() or
            reply.get_data().get_quality() == Tango::ATTR_INVALID);
  });
}

/**
 *  \brief Overload of extractUntilRepliesInvalid specifying the extracted type.
 *
 *  \tparam InputIt LegacyInputIterator (::value_type must be a
 *                  Tango::GroupReply)
 *  \tparam OutputIt LegacyOutputIterator used to extract data into
 *  \tparam DataType The type used for reply extraction before d_first insertion
 *
 *  \param[in] first, last The input iterator range of replies to copy
 *  \param[out] d_first The destination iterator range where to copy the data in
 *  \param[in] pred Unary predicate taking a reply as argument and returning
 *                  true when we should stop copying.
 *  \return pair<InputIt, OutputIt> Containing the first and d_first iterator
 *                                  where it stopped
 */
template <class DataType, class InputIt, class OutputIt>
std::pair<InputIt, OutputIt>
extractUntilRepliesInvalid(InputIt first, InputIt last, OutputIt d_first) {
  using reply_t = typename std::iterator_traits<InputIt>::value_type;

  static_assert(std::is_base_of<Tango::GroupReply, reply_t>::value,
                "extractRepliesUntilInvalid is only availaible for "
                "Tango::GroupReply inputs");

  const auto invalid_reply = [](reply_t &reply) {
    return (reply.has_failed() or
            reply.get_data().get_quality() == Tango::ATTR_INVALID);
  };
  return extractUntil<DataType>(first, last, d_first, invalid_reply);
}

/**
 *  \brief Copy Tango::GroupReply elements from [first, last) into d_first and
 *         fill d_first_invalid with the result of format_invalid(invalid_reply,
 *         d_first)
 *
 * This extract ALL valid replies from the [first, last) into d_first PLUS,
 * when a reply is invalid (has_failed() == true OR reply get_quality() ==
 * ATTR_INVALID), it calls a given format_invalid(*first, d_first) policy in
 * order to assign its return value to the d_first_invalid destination range.
 *
 * This can be used to filter valid replies and invalid one easily, and, for
 * example, generate error message (if d_first_invalid contains error message)
 * in a one pass algorithm.
 *
 *  Example:
 *  --------
 *  // NOTE: Something are double attribute.
 *  auto all_replies = my_group.read_attribute("Something");
 *
 *  std::vector<double> valid_value_list(
 *    all_replies.size(),
 *    std::numeric_limits<double>::infinity());
 *
 *  // We want to create a error msg when the reply was invalid
 *  std::vector<std::string> error_msg_list;
 *  error_msg_list.reserve(all_replies.size());
 *
 *  // I have to do this because of the lambda below (I'm too lazy to type !)
 *  // NOTE that cpp17 doesn't need this since we can use auto in lambda params
 *  using reply_t = decltype(*std::begin(all_replies));
 *  using out_it_t = decltype(std::begin(valid_value_list));
 *
 *  const auto format_err_msg = [](reply_t &invalid_reply){
 *    std::string err_msg = "Uh oh ...'Something' from "
 *                          + invalid_reply.dev_name()
 *                          + " went wrong !\n";
 *
 *    //Keep in mind that invalid means that we either failedd (exception) OR
 *    //the attr quality was INVALID -> Check it
 *    err_msg += (invalid_reply.has_failed() ?
 *                invalid_reply.get_err_stack()[0].desc.in() :
 *                "INVALID ATTR QUALITY");
 *
 *    return err_msg + '\n';
 *  };
 *
 * // If you want to use std::back_insert(valid_value_list), use the
 * // copyRepliesAndInvalid<double>
 *  auto out = copyRepliesAndInvalid(std::begin(all_replies),
 *                                   std::end(all_replies),
 *                                   valid_value_list.begin(),
 *                                   std::back_inserter(error_msg_list),
 *                                   format_err_msg);
 *
 *  //From here, if error_msg_list is empty -> no exception not invalid
 *  if(not error_msg_list.empty()) {
 *    Tango::Except::throw_exception(
 *      "Something",
 *      std::accumulate(error_msg_list.cbegin(),
 *                      error_msg_list.cend(),
 *                      std::string("Reading Something attributes failed:\n")),
 *      __PRETTY_FUNCTION__);
 *  }
 *
 *  \tparam InputIt LegacyInputIterator (::value_type must be a
 *                  Tango::GroupReply)
 *  \tparam OutputIt1 LegacyOutputIterator used to extract valid data into
 *  \tparam OutputIt2 LegacyOutputIterator used to invalid data into
 *  \tparam BinaryOp Function object type used to create value assigned to
 *                   d_first_invalid
 *
 *  \param[in] [first, last) The input iterator range of replies to copy
 *  \param[out] d_first_valid The destination iterator range where to copy the
 *                            valid data in
 *  \param[out] d_first_invalid The destination iterator range where to copy the
 *                              invalid data in
 *  \param[in] format_invalid A function that takes the element of [first,
 *                             last), and the corresponding d_first, in order to
 *                             format the error that will be assigned to
 *                             err_first
 *  \return pair<OutputIt1, OutputIt2> A pair constructed from the iterator to
 *                                     the end of d_first_valid and
 *                                     d_first_invalid range.
 */
template <class InputIt, class OutputIt1, class OutputIt2, class BinaryOp>
std::pair<OutputIt1, OutputIt2>
extractRepliesAndInvalid(InputIt first, InputIt last, OutputIt1 d_first_valid,
                         OutputIt2 d_first_invalid, BinaryOp format_invalid) {
  std::tie(first, d_first_valid) =
      extractUntilRepliesInvalid(first, last, d_first_valid);

  while (first != last) {
    *d_first_invalid++ = format_invalid(*first++);
    std::tie(first, d_first_valid) =
        extractUntilRepliesInvalid(first, last, d_first_valid);
  }
  return std::make_pair(d_first_valid, d_first_invalid);
}

/**
 *  \brief Overload of extractRepliesAndInvalid specifying the extracted type
 *
 *  \tparam InputIt LegacyInputIterator (::value_type must be a
 *                  Tango::GroupReply)
 *  \tparam OutputIt1 LegacyOutputIterator used to extract valid data into
 *  \tparam OutputIt2 LegacyOutputIterator used to invalid data into
 *  \tparam BinaryOp Function object type used to create value assigned to
 *                   d_first_invalid
 *  \tparam DataType The type used for reply extraction before d_first_valid
 *                   insertion
 *
 *  \param[in] [first, last) The input iterator range of replies to copy
 *  \param[out] d_first_valid The destination iterator range where to copy the
 *                            valid data in
 *  \param[out] d_first_invalid The destination iterator range where to copy the
 *                              invalid data in
 *  \param[in] format_invalid A function that takes the element of [first,
 *                             last), and the corresponding d_first, in order to
 *                             format the error that will be assigned to
 *                             err_first
 *  \return pair<OutputIt1, OutputIt2> A pair constructed from the iterator to
 *                                     the end of d_first_valid and
 *                                     d_first_invalid range.
 */
template <class DataType, class InputIt, class OutputIt1, class OutputIt2,
          class BinaryOp>
std::pair<OutputIt1, OutputIt2>
extractRepliesAndInvalid(InputIt first, InputIt last, OutputIt1 d_first_valid,
                         OutputIt2 d_first_invalid, BinaryOp format_invalid) {
  std::tie(first, d_first_valid) =
      extractUntilRepliesInvalid<DataType>(first, last, d_first_valid);

  while (first != last) {
    *d_first_invalid++ = format_invalid(*first++);
    std::tie(first, d_first_valid) =
        extractUntilRepliesInvalid<DataType>(first, last, d_first_valid);
  }
  return std::make_pair(d_first_valid, d_first_invalid);
}

/**
 *  \brief Overload of copyRepliesAndInvalid but additionaly push a default
 *         value into d_first_valid when an invalid reply is detected.
 *
 *  \note This means that the d_first_valid range will ALWAYS have the same size
 *  of the input range [first, last).
 *
 *  \note The type of the default value will be uses for extracting the data
 *  from the replies
 *
 *  \tparam InputIt LegacyInputIterator (::value_type must be a
 *                  Tango::GroupReply)
 *  \tparam OutputIt1 LegacyOutputIterator used to extract valid data into
 *  \tparam OutputIt2 LegacyOutputIterator used to invalid data into
 *  \tparam BinaryOp Function object type used to create value assigned to
 *                   d_first_invalid
 *  \tparam T Type of the data inserted into d_first_valid when an invalid
 *            reply is detected
 *
 *  \param[in] [first, last) The input iterator range of replies to copy
 *  \param[out] d_first_valid The destination iterator range where to copy the
 *                            valid data in
 *  \param[out] d_first_invalid The destination iterator range where to copy the
 *                              invalid data in
 *  \param[in] format_invalid A function that takes the element of [first,
 *                             last), and the corresponding d_first, in order to
 *                             format the error that will be assigned to
 *                             err_first
 *  \param[in] valid_default The default added to d_first_valid when a reply is
 *                           invalid
 *  \return pair<OutputIt1, OutputIt2> A pair constructed from the iterator to
 *                                     the end of d_first_valid and
 *                                     d_first_invalid range.
 */
template <class T, class InputIt, class OutputIt1, class OutputIt2,
          class BinaryOp>
std::pair<OutputIt1, OutputIt2>
extractRepliesAndInvalid(InputIt first, InputIt last, OutputIt1 d_first_valid,
                         OutputIt2 d_first_invalid, BinaryOp format_invalid,
                         const T &valid_default) {
  std::tie(first, d_first_valid) =
      extractUntilRepliesInvalid<T>(first, last, d_first_valid);

  while (first != last) {
    *d_first_invalid++ = format_invalid(*first++);
    *d_first_valid++ = valid_default;
    std::tie(first, d_first_valid) =
        extractUntilRepliesInvalid<T>(first, last, d_first_valid);
  }
  return std::make_pair(d_first_valid, d_first_invalid);
}

} // namespace utils
